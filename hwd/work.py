# Copyright (c) 2016 Marco Marinello

# Importing django utils
from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

# Importing models
from django.contrib.auth.models import User
from models import Pages, Attachment
import os
from htmlwebdeploy import settings

ATTACHS_DIR = os.path.join(settings.BASE_DIR, "attachments")
if not os.path.exists(ATTACHS_DIR):
    os.mkdir(ATTACHS_DIR)



# Work homepage
@login_required
def index(request):
    pages = Pages.objects.filter(owner=request.user).order_by("name")
    attachs = Attachment.objects.filter(owner=request.user).order_by("name")
    act = request.GET.get("act")
    return render(request,
                  "hwd/dashboard.html",
                  {'pages': pages,
                   'attachs': attachs})


# Create new page function
@login_required
def new_page(request):
    if not request.POST.get("pagename"):
        return redirect("work")
    user_obj = request.user
    pagename = request.POST.get("pagename")
    Pages.objects.create(owner=user_obj,
                      name=pagename)
    return redirect(reverse('work-edit-page')+"?page="+pagename)


# Edit page function
@login_required
def edit_page(request):
    try:
        page = Pages.objects.get(
            owner=request.user,
            name=request.GET.get("page")
            )
    except:
        return redirect("work")

    return render(request,
                  "hwd/pagedit.html",
                  {'pagename': page.name,
                   'pagemod': page.last_modify,
                   'pagecont': page.content})


# Save page function
@login_required
def save_page(request):
    if not request.POST:
        return redirect("work")
    try:
        page = Pages.objects.get(owner=request.user,
                                 name=request.POST["page"]
                                 )
    except:
        return redirect("work")
    page.content = request.POST["content"]
    page.save()
    messages.success(request, _("Page successfully saved"))
    return redirect(
        reverse('work-edit-page')+"?page=%s" % page.name
        )

# Upload page function (view)
@login_required
def upload_html_view(request):
    return render(request, "hwd/upload-html.html")


# Do page uploading function
@login_required
def do_html_upload(request):
    if not request.POST or not request.FILES:
        return redirect("work")

    attach = request.FILES["attach"]
    name = request.POST["name"]

    Pages.objects.create(
        owner=request.user,
        name=name,
        content=attach.read()
        )

    return redirect(
        reverse("work-edit-page") + "?page=" + name
        )


# Drop a page function
@login_required
def drop_page(request):
    if not request.GET:
        return redirect("work")
    try:
        name = request.GET["pagename"]
        pg_obj = Pages.objects.get(
            owner=request.user,
            name=name
            )
    except:
        return redirect("work")
    pg_obj.delete()
    return redirect(
        reverse("work") + "?act=pdropd"
        )


# Upload attach page
@login_required
def new_attach(request):
    ctx = {
        'error': request.GET.get("error"),
        }
    return render(request, "hwd/upload-attach.html", ctx)


# Upload completetion
@login_required
def attach_upload(request):
    if not request.FILES or not request.POST:
        return redirect("work")
    attach = request.FILES["attach"]
    name = request.POST["name"]

    # Check that the filename does not content spaces
    if " " in name:
        name = name.replace(" ", "")

    # Attach extension check
    e = attach.name.split(".")[-1]
    
    if e not in settings.ALLOWED_EXTENSIONS:
        return redirect(reverse("work-new-attach")+"?error=notau")

    Attachment.objects.create(
        owner=request.user,
        name=name,
        attach=attach
        )
    return redirect("work")


# View an attach function
@login_required
def attach_view(request):
    if not request.GET:
        return redirect("work")
    try:
        attach = Attachment.objects.get(
            owner=request.user,
            name=request.GET["name"]
            )
    except:
        return redirect("work")
    return render(request,
                  "hwd/view-attach.html",
                  {"attach": "/users/%s/%s" % (
                      request.user.username,
                      attach.name
                      ),
                   "attachname": attach.name
                   }
                  )


# Drop attach function
@login_required
def drop_attach(request):
    if not request.GET:
        return redirect("work")
    try:
        obj = Attachment.objects.get(
            owner=request.user,
            name=request.GET["name"]
            )
    except:
        return redirect("work")
    try:
        os.remove(obj.attach.path)
    except:
        pass
    obj.delete()
    return redirect("work")

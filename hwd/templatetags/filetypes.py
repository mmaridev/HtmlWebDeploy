# Copyright (c) 2016 Marco Marinello

from django import template

register = template.Library()


def _get_filetype_icon(name):
    if "." not in name:
        return None
    des = name.split(".")[-1]
    if des in ("png", "bmp", "jpg", "jpeg", "xcf"):
        return "img/image.png"
    return "img/%s.png" % des

register.filter('fileicon', _get_filetype_icon)


def _is_image(name):
    des = name.split(".")[-1]
    if des in ("png", "bmp", "jpg", "jpeg", "xcf"):
        return True
    return False

register.filter('is_image', _is_image)

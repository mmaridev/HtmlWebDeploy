# Copyright (c) 2016 Marco Marinello


from django import template
from htmlwebdeploy import settings


register = template.Library()


def _get_var_from_settings(varname):
    if varname == "VERSION":
        return settings.VERSION
    elif varname == "CODENAME":
        return settings.CODENAME
    else:
        return "Cannot access settings"


register.filter('settings_var', _get_var_from_settings)

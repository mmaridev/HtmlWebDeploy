# Copyright (c) 2016 Marco Marinello

from django import template
from hwd.views import _get_true_name


register = template.Library()
register.filter('get_name', _get_true_name)

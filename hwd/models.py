from django.db import models
from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _


class Pages(models.Model):
    owner = models.ForeignKey(
        User,
        null=False,
        verbose_name=_("user"),
        help_text=_("page owner")
        )

    name = models.CharField(
        max_length=30,
        verbose_name=_("page name"),
        help_text=_("filename of the page"),
        )

    last_modify = models.DateTimeField(
        auto_now=True,
        verbose_name=_("last modify")
        )

    content = models.TextField(
        verbose_name=_("content"),
        help_text=_("HTML page source")
        )

    def __unicode__(self, *args):
        return u"%s %s %s" % (self.name, _("of"), self.owner.get_full_name())

    class Meta:
        verbose_name = _("page")
        verbose_name_plural = _("pages")


# Calculate destintion of an attach function
def attach_destination(instance, filename):
    return "attachments/%s/%s" % (instance.owner.username,
                                   filename.replace(" ", ""))


class Attachment(models.Model):
    owner = models.ForeignKey(
        User,
        null=False,
        verbose_name=_("user"),
        help_text=_("attachment owner")
        )

    name = models.CharField(
        max_length=30,
        verbose_name=_("attachment name"),
        help_text=_("filename of the attach"),
        )

    last_modify = models.DateTimeField(
        auto_now=True,
        verbose_name=_("last modify")
        )

    attach = models.FileField(
        upload_to=attach_destination,
        verbose_name=_("attach")
        )

    def __unicode__(self, *args):
        return u'%s %s %s' % (self.name, _("of"), self.owner.get_full_name())

    class Meta:
        verbose_name = _("attachment")
        verbose_name_plural = _("attachments")

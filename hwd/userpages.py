# Copyright (c) 2016 Marco Marinello

# Import django required modules
from django.shortcuts import redirect, HttpResponse, render
from django.contrib.auth.models import User
import models


# Get true user's name function
def _get_true_name(user):
    return user.get_full_name() or user.username


# Page check and return function
def check_return_page(user, name, request):
    try:
        return HttpResponse(models.Pages.objects.get(
            owner=user,
            name=name
            ).content)
    except:
        if not models.Pages.objects.filter(
                owner=user, name=name):
            if name == "index.html":
                return render(
                    request,
                    "hwd/no-index.html",
                    {'username': _get_true_name(user)}
                )
            return False


# Attachment check and return function
def check_return_attach(user, name):
    try:
        a = models.Attachment.objects.get(
            owner=user,
            name=name).attach
        for i in (".png", ".jpg", ".bmp"):
            if i in name:
                return HttpResponse(
                    a,
                    content_type="image/%s" % i[1:]
                    )
        return HttpResponse(a)
    except:
        return False


# New get page function
def get_page(request, username=None, pagename=None, *args):
    # Check if minimum data were given
    if not username:
        return redirect("home")
    # Check if page name were given
    if not pagename:
        pagename = "index.html"
    # Rebuild full url (django may drop something)
    if "." not in pagename:
        pagename = request.build_absolute_uri().split("/")[-1]

    # Check if user exists
    try:
        user_obj = User.objects.get(username=username)
    except:
        # Except, return user-not-exists error
        return render(request,
                      "hwd/no-user.html",
                      {'username': username}
                      )

    # Check if a page is requested as first
    page = check_return_page(user=user_obj,
                             name=pagename,
                             request=request)
    if page:
        # Is a page, return the object preprocessed
        return page

    # Check if an attach is requested
    attach = check_return_attach(
        user=user_obj, name=pagename)
    if attach:
        # If an attach has been recived, return them
        return attach
    return HttpResponse(status=500)

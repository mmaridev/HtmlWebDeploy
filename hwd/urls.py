# Copyright (c) 2016 Marco Marinello

from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.decorators import login_required


# Normal patterns
import views
urlpatterns = [
    url(r'^$', views.main_welcome, name='home'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.go_logout, name='logout')
]


# User pages patterns
from userpages import get_page
users_patterns = [
    url(r'^users/(?P<username>[a-z]+)/$', get_page),
    url(r'^users/(?P<username>[a-z]+)/(?P<pagename>[a-zA-Z0-9_ ]*)?', get_page)
]

for i in users_patterns: urlpatterns.append(i)


# Work patterns
import work
work_patterns = [
    url(r'^work/$', work.index, name='work'),
    url(r'^work/newpage$', work.new_page, name='work-new-page'),
    url(r'^work/editpage$', work.edit_page, name='work-edit-page'),
    url(r'^work/savepage$', work.save_page, name='work-save-page'),
    url(r'^work/uploadhtml$', work.upload_html_view, name='work-upload-page'),
    url(r'^work/htmlup$', work.do_html_upload, name='work-do-html-upload'),
    url(r'work/dropage$', work.drop_page, name='work-drop-page'),
    url(r'^work/newattach$', work.new_attach, name='work-new-attach'),
    url(r'^work/uploadattach$', work.attach_upload, name='work-attach-upload'),
    url(r'^work/viewattach$', work.attach_view, name='work-attach-view'),
    url(r'^work/dropattach$', work.drop_attach, name='work-attach-drop')
]

for i in work_patterns: urlpatterns.append(i)

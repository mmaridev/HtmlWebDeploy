from django.contrib import admin
import models


class PagesAdmin(admin.ModelAdmin):
    search_fields = [
        'owner__username',
        'name',
        'content'
        ]

    list_display = ['owner', 'name', 'last_modify']
    list_display_links = ['owner']

admin.site.register(models.Pages, PagesAdmin)


class AttacchAdmin(admin.ModelAdmin):
    search_fields = [
        'owner__username',
        'name'
        ]

    list_display = ['owner', 'name', 'last_modify']
    list_display_links = ['owner']

admin.site.register(models.Attachment, AttacchAdmin)

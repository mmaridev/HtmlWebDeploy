# Copyright (c) 2016 Marco Marinello

# Importing django utils
from django.shortcuts import render, HttpResponse, redirect
from django.contrib import auth
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

# Import true user's name function
from userpages import _get_true_name

# Importing models
from django.contrib.auth.models import User
from models import Pages, Attachment

# Some danger message
def add_danger(request, message):
    messages.add_message(request, "50", message, extra_tags="danger")
messages.danger = add_danger


# / index
def main_welcome(request):
    if request.user.is_authenticated():
        return redirect("work")
    return render(request,
                  "hwd/main.html",
                  {'next': request.GET.get("next", "")}
                  )


# login
def login(request):
    if not request.POST:
        return redirect("home")
    udata = [request.POST.get("username"),
             request.POST.get("password")]
    check = auth.authenticate(
        username=udata[0],
        password=udata[1])
    if check:
        if check.is_active:
            auth.login(request, check)
            messages.success(request, "%s, %s" % (
                _("Welcome"), _get_true_name(check)
                ))
            return redirect("work")
    else:
        messages.danger(request, _("Wrong username or password"))
        return redirect("home")


# Logout function
def go_logout(request):
    logout(request)
    if not request.user.is_authenticated():
        messages.success(request, _("Logged out successfully!"))
    return redirect("home")


# Import views from other modules
from userpages import get_page

# HTMLWebDeploy

## è un software che permette di vedere in real-time il codice HTML scritto


## Releases:

### v. 0.1 Martin

## Future releases (in develop):

### v. 1.0


## Come incominciare

### Installare PyPi

*sudo apt-get install python-pip git*

### Installare Django

*pip install Django*

### Creare una chiave ssh ed aggiungerla in gitlab con

*ssh-keygen*

Poi copiare l'output di *cat .ssh/id_rsa.pub* ed incollarlo nelle preferenze del profilo, nella sezione SSHKeys

### Scaricare il progetto con *git clone git@gitlab.com:mmaridev/HtmlWebDeploy*

Dalla shell entrare nella cartella quindi eseguire

*python manage.py makemigrations hwd*

*python manage.py migrate*

Ciò creerà il database per consentirvi di lavorare. Ora però vi serve un utente

*python manage.py createsuperuser*

e rispondete alle domande. Arrivati a questo punto potete eseguire il server

*python manage.py runserver*

e l'indirizzo sarà *localhost:8000* (accessibile solo da localhost)

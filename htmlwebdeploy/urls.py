# Copyright (c) 2016 Marco Marinello

from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^', include('hwd.urls')),
    url(r'^admin/', admin.site.urls, name='admin'),
]
